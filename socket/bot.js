

const botData = require('../data/bot.text');
const random = require('random');
const startMessage = botData.start;
let sortPlayers = require('./helpers').sortPlayers;
const facts = botData.facts;
//singleton
let bot = (function () {
    let socket;
    let io;
    let instance;
    let messageInterval;
    let timeToFact = -5;//sec
    let sendMessage = message => io.to('game').emit('message from bot', message);
    let liderBoardInterval = 30;//sec
    return {
        initializeBot: function () {
            if (instance) return instance;
            else {
                instance = {};
                [io, socket] = arguments;

                return instance;
            }
        },

        start: (players) => {
            console.log('bot stated');

            io.to('game').emit('message from bot', startMessage);

            setTimeout(() => {
                let participant = (login, position) => `${login} под номером ${position + 1}`;
                let message = 'Список участников: ';
                Object.values(players).forEach((player, position) => {
                    message += participant(player.login, position)+' ';
                })
                sendMessage(message);
            }, 4000);

            messageInterval = setInterval(() => {
                timeToFact = 0;
                let sortedPlayersArray = sortPlayers(players);
                let [player1, player2] = sortedPlayersArray;
                let message = `Лидирует ${player1.login} за ним следует ${player2.login} на расстоянии ${player1.score - player2.score}`;
                sendMessage(message)
            }, liderBoardInterval * 1000);
            factInterval = setInterval(() => {
                timeToFact++;
                if (timeToFact == 7) {
                    let message = `Интересный факт: ${facts[Math.floor(random.int(0, facts.length - 1))]}`;
                    console.log('fact')
                    sendMessage(message);
                    timeToFact = 0;
                }
            }, 1000);

        },

        stop: (players) => {
            clearInterval(messageInterval);
            clearInterval(factInterval);
            let sortedPlayersArray = sortPlayers(players);
            //proxy
            let winners = new Proxy(sortedPlayersArray, {
                get(target, prop) {
                    return target[prop].login;
                },
            });
            let place = (num) => sortedPlayersArray.length >= num ? `${num} место - ${winners[num - 1]}` : "";
            let message = `Результаты игры: ${place(1)}  ${place(2)}  ${place(3)}  `
            sendMessage(message)

        },

        finish: (player) => {
            timeToFact = 0;
            let message = `Игрок ${player.login} завершил гонку. Его время - ${player.time} c.  `;
            sendMessage(message);

        },
        finishIsNear: (player) => {
            timeToFact = 0;
            let message = `Игрок ${player.login} приближается к финишу`;
            sendMessage(message)
        }

    }
})()


module.exports = bot;