const jwt = require('jsonwebtoken');
const bot = require('./bot');
const dictionary = require('../repositories/repository').getDictionary();
const dictionaryLength = dictionary.length;
let players = {};
let isGameStarted = false;
let isReadyToStart = false;
let finishedPlayers = 0;
const timeToStart = 5000; //ms
module.exports = (io, socket) => {

    socket.on('readyToStart', (payload) => {
        if (!isGameStarted) {
            socket.join('game', () => {
                let { token } = payload;
                const login = jwt.decode(token).login;
                players[socket.id] = { login, score: 0, time: 0 };
                let clientNumber = io.sockets.adapter.rooms['game'].length;
                if (clientNumber > 1 && !isReadyToStart) {
                    finishedPlayers = 0;
                    isReadyToStart = true;
                    io.emit('prepare to start');
                    console.log(players)
                    let preparetoStart = setTimeout(() => {
                        isGameStarted = true;
                        io.to('game').emit('start game', { players });
                        bot.start(players)
                        io.emit('endTime');
                    }, timeToStart + 1000)
                }
                console.log('clientNumber', clientNumber);
            });
        }

    })


    socket.on('disconnect', () => {
        if (players[socket.id]) {
            players[socket.id].disconected = true;
            players[socket.id].score = 0;
        }
        io.to('game').emit('update score', { players });
        if (Object.values(players).length <= 1) {
            isGameStarted = false;
            isReadyToStart = false;
        }
    });

    socket.on('game status', (payload) => {
        const { token } = payload;
        let player = players[socket.id]
        if (player) {
            player.score++;
            if (dictionaryLength - player.score == 30) {
                bot.finishIsNear(player);
            }
        }

        io.to('game').emit('update score', { players });
    })

    socket.on('who won?', (payload) => {
        const { time } = payload;
        if (players[socket.id]) {
            players[socket.id].time = time;

            bot.finish(players[socket.id]);
        }
        finishedPlayers++;
        if (io.sockets.adapter.rooms['game'] && finishedPlayers == io.sockets.adapter.rooms['game'].length) {
            io.to('game').emit('end game', { players });
            bot.stop(players);
            players = {};
            isGameStarted = false;
            isReadyToStart = false;
        }
    })

};

