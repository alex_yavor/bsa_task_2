const sortPlayers = function (players) {
    const sotrtedByScoreAndTime = (player1, player2) => {
        const { score: score1, time: time1 } = player1;
        const { score: score2, time: time2 } = player2;
        if (score1 != score2) {
            return score2 - score1;
        }
        else {
            return time1 - time2;
        }
    }
    return Object.values(players).sort(sotrtedByScoreAndTime);
}

module.exports = {
    sortPlayers
}