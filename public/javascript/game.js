let socket = io.connect('http://localhost:3000');
let dictionaryLength = 0;
let arrayDictionary = [];
let dictionary = "";
window.onload = () => {
    const jwt = localStorage.getItem('jwt');
    if (!jwt) {
        location.replace('/login');
    } else {

        const readyButton = document.querySelector(".ready-button");
        const status = document.querySelector('.status');
        let timeToEnd = 0;// 200c/m - average time typing, 0.3 sec/char
        let gameTime = 0;
        let timeToStart = 5;//sec
        let timeToReload=5;//sec
        fetch('/game', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${jwt}`
            },

        }).then(res => {
            console.log(res)
            res.json().then(body => {
                dictionary = body.dictionary;
                arrayDictionary = dictionary.split('');
                dictionaryLength = arrayDictionary.length;
                gameTime = timeToEnd = Math.round(dictionaryLength * 0.3);
            })
        }).catch(err => {
            console.log('request went wrong');
        })

        readyButton.addEventListener('click', ev => {
            socket.emit("readyToStart", { token: jwt })
        })

        socket.on('prepare to start', async (params) => {

            let prepareToGame = setInterval(() => {
                if (timeToStart <= 0) clearInterval(prepareToGame);
                status.innerHTML = `Time to next game: ${timeToStart--} `;
            }, 1000)
        })

        socket.on('start game', async (params) => {
            const gameArea = document.querySelector('.game-area');
            readyButton.style.display = 'none';
            status.style.display = 'none';
            gameArea.innerHTML = dictionary;
            const { players } = params;
            addProgressBar({ players, dictionaryLength });

            document.onkeypress = function (evt) {
                evt = evt || window.event;
                const charCode = evt.keyCode || evt.which;
                const insertedChar = String.fromCharCode(charCode);
                const curChar = arrayDictionary[0];
                if (curChar == insertedChar) {
                    arrayDictionary.shift();
                    //facade
                    updateGameArea();

                    if (arrayDictionary.length == 0) {
                        console.log('eeebee')
                        socket.emit('who won?', { token: jwt, time: gameTime - timeToEnd });
                    }
                    socket.emit('game status', { token: jwt })
                }
            };

        })


        socket.on('endTime', () => {
            let timeDiv = document.querySelector('.time');
            let timerToEnd = setInterval(() => {
                timeDiv.innerHTML = `until the end ${timeToEnd--} second(s)`;
                if (timeToEnd <= -1) {
                    document.onkeypress = null;
                    console.log('eeeee')
                    socket.emit('who won?', { token: jwt, time: gameTime - timeToEnd });
                    clearInterval(timerToEnd);
                }
            }, 1000)
        })

        socket.on('update score', ({ players }) => {
            addProgressBar({ players, dictionaryLength });
        })

        socket.on('end game', ({ players }) => {
            console.log('end ', players)
            let winner = { name: "", score: -1 };
            Object.values(players).forEach((player) => {
                const progres = document.querySelector(`#${player.login}`);
                if (player.score > winner.score) winner = player;
                progres.setAttribute('value', player.score);

            })
            status.style.display = "block";
            status.innerHTML = `${winner.login} WON!!!`;
            setTimeout(() => {
                location.reload();
            }, timeToReload*1000);
        })
    }

    function addProgressBar({ players, dictionaryLength }) {
        let playersDiv = document.querySelector('.players');
        playersDiv.innerHTML = "";
        Object.values(players).forEach((player) => {
            let login = player.login;
            let progress = document.createElement('progress');
            progress.setAttribute("value", player.score);
            progress.setAttribute("max", dictionaryLength);
            progress.setAttribute("id", login);
            let nameDiv = document.createElement('div');
            //factory
            nameDiv.innerHTML = NameDivText(player)
            playersDiv.append(progress, nameDiv);
        });

    }

}

