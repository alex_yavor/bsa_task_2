  //facade    
  function updateGameArea() {
    const gameArea = document.querySelector('.game-area');
    const completedElement = createCompletedPart();
    const nextElement = createNextPart();
    const currentElement = createCurrentPart();
    gameArea.innerHTML = "";
    gameArea.appendChild(completedElement);
    gameArea.appendChild(currentElement);
    gameArea.appendChild(nextElement);
   
}

function createCompletedPart() {
    const currCharPos = dictionaryLength - arrayDictionary.length - 1;
    const completedPart = dictionary.substring(0, currCharPos + 1);
    let completedElement = document.createElement('span');
    completedElement.setAttribute('class', 'compl-element');
    completedElement.innerHTML = completedPart;
    return completedElement;
}

function createNextPart() {
    const currCharPos = dictionaryLength - arrayDictionary.length - 1;
    const nextPart = dictionary.substring(currCharPos + 2);
    let nextElement = document.createElement('span');
    nextElement.innerHTML = nextPart;
    return nextElement;
}

function createCurrentPart() {
    const currCharPos = dictionaryLength - arrayDictionary.length - 1;
    const currentElement = document.createElement('span');
    currentElement.setAttribute('class', 'curr-element');
    currentElement.innerHTML = dictionary.substring(currCharPos + 1, currCharPos + 2);
    return currentElement;
}



//factory
const NameDivText = (player) => {
    if (player.disconected) {
        return disconectedUser(player)
    }
    if (player.time > 0) {
        return finishedUser(player);
    }
    else {
        return user(player);
    }
}

const user = (player) => {
    return `${player.login}`;
}

const disconectedUser = (player) => {
    return `${player.login} disconected`;
}

const finishedUser = (player) => {
    return `${player.login} finished in ${player.time}`;
}