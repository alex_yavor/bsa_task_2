const express = require('express');
const router = express.Router();
const path = require('path');
const users = require('../data/userlist');
const jwt = require('jsonwebtoken');

router.get('/', (req, res, next) => {
  res.redirect('/game');
});

router.get('/login', (req, res, next) => {
  res.sendFile(path.join(__dirname, '../login.html'));
});

router.post('/login', (req, res, next) => {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'someSecret', { expiresIn: '24h' });
    res.status(200).json({ auth: true, token });
  } else {
    res.status(401).json({ auth: false });
  }
});

module.exports = router;
