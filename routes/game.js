const express = require('express');
const router = express.Router();
const path = require('path');
const dictionary = require('../repositories/repository').getDictionary();
const passport = require('passport');

router.get('/', async (req, res, next) => {
    res.sendFile(path.join(__dirname, '../game.html'));
});

router.post('/', passport.authenticate('jwt', { session: false }), async (req, res, next) => {
    res.status(200).json({ dictionary });
});


module.exports = router;
