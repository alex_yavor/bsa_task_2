const fs = require('fs');
const random = require('random');
const getDictionary = () => {
    const dict = require('../data/dict');
    const len = dict.length;
    let rnd = random.int(0, len - 1);
    return dict[rnd];
}


module.exports = {
    getDictionary

};