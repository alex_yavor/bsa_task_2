const express = require('express');
const bodyPaser = require('body-parser');
const logger = require('morgan');

const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');

const indexRouter = require('./routes/index');
const gameRouter = require('./routes/game');


require('./middlewares/passport.config');

server.listen(3000);
io.on('connection', socket => {
    require('./socket/index')(io, socket);
    require('./socket/bot').initializeBot(io, socket);
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyPaser.json())
app.use(bodyPaser.urlencoded({ extended: false }))
app.use(express.static('public'));

app.use(passport.initialize());


app.use('/', indexRouter);
app.use('/game', gameRouter);


// module.exports = app;
module.exports = server;
